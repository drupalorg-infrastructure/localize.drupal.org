<?php

$update_free_access = FALSE;

$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

// Disable poormanscron & link theming.
$conf['cron_safe_threshold'] = 0;
$conf['theme_link'] = FALSE;

// Set proper temporary path - on NFS, outside of http-servable area.
$conf['file_temporary_path'] = '../files-tmp';

// Drupal.org configuration.
$conf['drupalorg_site'] = 'localize';
$conf['drupalorg_crosssite_lock_permissions'] = TRUE;

// Increase memory limit for parsing.
// modules/simpletest/tests/upgrade/drupal-6.filled.database.php in Drupal 7
// choked at 400M. Newer versions of PHP will mitigate this. Or, trim Grammar
// Parser.
if (defined('DRUSH_SUCCESS')) {
  ini_set('memory_limit', '2048M');
}

/**
 * Include a common settings file if it exists.
 */
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

/**
 * Include a local settings file if it exists.
 */
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
