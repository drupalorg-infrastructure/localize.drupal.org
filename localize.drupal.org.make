core = 7.x
api = 2
projects[drupal][version] = "7.103"


; Modules
projects[ctools][version] = "1.15"

projects[diff][version] = "3.4"

projects[entity][version] = "1.9"

projects[entityreference][version] = "1.5"

projects[entityreference_prepopulate][version] = "1.3"

projects[features][version] = "2.11"

projects[field_extrawidgets][version] = "1.1"

projects[google_analytics][version] = "2.6"

projects[l10n_pconfig][version] = "1.2"

projects[l10n_server][version] = "1.x-dev"
projects[l10n_server][download][revision] = "b5a8a5e"

projects[link][version] = "1.11"

projects[og][version] = "2.9"

; https://www.drupal.org/node/1985800#comment-7802723: Set default value of og_roles_permissions field during migration
projects[og][patch][] = "https://www.drupal.org/files/issues/1985800-15-og_global_roles_permissions.patch"

; https://www.drupal.org/node/2349693#comment-9210849: Missing message after automatically joining a group
projects[og][patch][] = "https://www.drupal.org/files/issues/2349693-3-missing_message_subscribe.patch"

; https://www.drupal.org/node/2540984#comment-10159446: OG migration doesn't properly map the changed field
projects[og][patch][] = "https://www.drupal.org/files/issues/2540984-og_user-created-1.patch"

; https://www.drupal.org/node/1424984#comment-10159986: Revert Two OG context patches that make LDO groups fail.
projects[og][patch][] = "https://www.drupal.org/files/issues/og-revert-context-init-9685ef6.patch"
projects[og][patch][] = "https://www.drupal.org/files/issues/og-revert-context-user-group-access-c5b90d8c.patch"

projects[openid_connect][version] = "1.3"
; Support to logout from the OAuth2 Server provider
; https://www.drupal.org/project/openid_connect/issues/2882270
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-107.diff"
; Add optional username and email synchronization from identity provider
; https://www.drupal.org/project/openid_connect/issues/3452530
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-111.diff"
; Trying to get property of non-object in locale_language_url_rewrite_url()
; https://www.drupal.org/project/openid_connect/issues/3464573
projects[openid_connect][patch][] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org/raw/HEAD/patches/openid_connect-119.diff"

projects[potx][version] = "3.x-dev"
projects[potx][download][revision] = "3e43833"

projects[tag1_d7es][version] = "1.2"

projects[views_bulk_operations][version] = "3.4"

projects[views][version] = "3.21"

; Custom module
projects[localizedrupalorg][version] = "1.x-dev"
projects[localizedrupalorg][download][revision] = "5ba1027"

;; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
